import json
import requests
import pymongo
from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponse
from rest_framework.parsers import JSONParser
from django.views.decorators.csrf import csrf_exempt
from .models import Loginuserpassword
from .serializers import UserSerializer


def index(request):
    url = "https://randomuser.me/api"
    r = requests.get(url)
    json_data = r.json()

    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["django"]
    mycol = mydb["myJson"]

    if mycol.count() != 0:
        myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        mydb = myclient["django"]
        mycol = mydb["myJson"]
        mycol.drop()

        myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        mydb = myclient["django"]
        mycol = mydb["myJson"]
        x = mycol.insert_many(json_data['results'])

    return render(request, 'userInfo/userInfo.html')


@csrf_exempt
def registeruser(request):
    if request.method == 'POST':
        user_data = JSONParser().parse(request)
        user_serializer = UserSerializer(data=user_data)
        if user_serializer.is_valid():
            user_serializer.save()
            response_data = {}
            response_data["success"] = True
            return HttpResponse(json.dumps(response_data), content_type="application/json")

    else:
        print("its not post")
        return HttpResponse(json.dumps({"username": username, "success": False}), content_type="application/json")

    # return render(request, 'userInfo/testAPI.html')
    return HttpResponseBadRequest()


@csrf_exempt
def login(request):
    user_data = JSONParser().parse(request)
    user_serializer = UserSerializer(data=user_data)
    loginFromAPI = user_serializer.initial_data['username']
    loginPassdFromAPI = user_serializer.initial_data['password']
    userInfoFromDB = Loginuserpassword.objects.filter(username=loginFromAPI)
    userDBInitializer = UserSerializer(userInfoFromDB, many=True)
    userPasswdFromDB = userDBInitializer.data[0]['password']
    if loginPassdFromAPI == userPasswdFromDB:
        response_data = {}
        # response_data["api_key"] = str(api_key).split(" ")[0]
        response_data["success"] = True
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        return HttpResponse(json.dumps({"username": username, "success": False}), content_type="application/json")
    return HttpResponseBadRequest()


@csrf_exempt
def getUserData(request):
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["django"]
    mycol = mydb["myJson"]
    usersData = mycol.find()
    item = []

    for x in mycol.find({}, {"_id": 0, "name": 1, "email": 1}):
        email = x['email']

        fullname = x['name']['title'] + ' ' + x['name']['first'] + ' ' + x['name']['last']
        print(" title doc is", fullname)

        item = {
            "name": fullname,
            "email": email
        }
        return JsonResponse(item)

    return HttpResponseBadRequest()


@csrf_exempt
def getUserDetail(request, email):
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["django"]
    mycol = mydb["myJson"]
    email = "manuel.duran@example.com"

    for x in mycol.find():
        if x['email'] == email:
            fullname = x['name']['title'] + ' ' + x['name']['first'] + ' ' + x['name']['last']
            gender = x['gender']
            email = x['email']
            location = x['location']['street']['number'], x['location']['street']['name'] + ' ' + x['location'][
                'city'] + ' ' + x['location']['state']

            userdetail = {
                "name": fullname,
                "gender": email,
                "email": email,
                "location": location
            }
            return JsonResponse(userdetail, content_type="application/json")

    # print("user detail", userDetail)

    return HttpResponseBadRequest()
