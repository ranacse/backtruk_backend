from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path, include
from randonUser import views
from rest_framework.authtoken.views import ObtainAuthToken

urlpatterns = [
    path('', views.index),
    path('admin/', admin.site.urls),
    url(r'^auth/', ObtainAuthToken.as_view()),
    path(r'registeruser/', views.registeruser),
    path('login/', views.login),
    path('getUserData/', views.getUserData),
    # url(r'getUserDetail/(?P<user_id>\d+)/$', views.getUserDetail),
    path('getUserDetail/<str:email>', views.getUserDetail)

]
